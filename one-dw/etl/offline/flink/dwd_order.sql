create table ods_order
(
    id          int primary key,
    user_sn     string,
    user_phone  string,
    order_sn    string,
    goods_sn    string,
    goods_num   bigint,
    order_price decimal(16, 4),
    create_time string,
    update_time string,
    ds          string
) PARTITIONED BY (ds)
WITH ( 'connector' = 'hudi',
    'path' = 'hdfs://one:9000/user/datalake/dw.db/ods_order',
    'table.type' = 'MERGE_ON_READ',
    'read.streaming.enabled' = 'false');


create table ods_goods
(
    id          int
        primary key,
    goods_sn    string,
    goods_name  string,
    orgin_price decimal(16, 4),
    sale_price  decimal(16, 4),
    create_time string,
    update_time string
) WITH ( 'connector' = 'hudi',
      'path' = 'hdfs://one:9000/user/datalake/dw.db/ods_goods',
      'table.type' = 'MERGE_ON_READ',
      'read.streaming.enabled' = 'false');
create table dwd_order
(
    id          int primary key,
    user_sn     string,
    user_phone  string,
    order_sn    string,
    goods_sn    string,
    goods_num   bigint,
    order_price decimal(16, 4),
    create_time string,
    update_time string,
    ds          string
) PARTITIONED BY (ds)
WITH ( 'connector' = 'hudi',
    'path' = 'hdfs://one:9000/user/datalake/dw.db/dwd_order',
    'table.type' = 'MERGE_ON_READ',
    'hive_sync.enable'='true',
    'hive_sync.table'='dwd_order',
    'hive_sync.db'='dw',
    'hive_sync.mode' = 'hms',
    'hive_sync.metastore.uris' = 'thrift://one:9083'
    );

insert into dwd_order
select *
from ods_order t1
         left join ods_goods t2
                   on t1.goods_sn = t2.goods_sn
;



create table t1
(
    id int primary key
)
WITH ( 'connector' = 'hudi',
    'path' = 'hdfs://one:9000/tmp/hudi/t1',
    'table.type' = 'MERGE_ON_READ'
    );

alter table t1 alter column id type string;


