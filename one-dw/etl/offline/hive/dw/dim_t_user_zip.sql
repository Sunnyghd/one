-- 初始化数据，执行一次任务即可
insert
overwrite table dim_t_user_zip
select *
     , dd           as start_date
     , '9999-99-99' as end_date
from ods.tbl_users
where dd = '2022-10-17';
---------------------------------------------------------
-- 构建拉链，每天执行
insert
overwrite table dw.dim_t_user_zip
-- 当天分区新增的数据
select *
     , dd           as start_date
     , '9999-99-99' as end_date
from ods.tbl_users
where
where dd='2022-10-18'
-- 前一天分区被当天分区修改的数据
union all
select t1.*
     , t1.start_date as start_date
     , if(t2.id is not null and t1.end_date = '9999-99-99', date_add(t2.dd, -1) -- 因为是t+1的任务
    , t1.end_date)   as end_date
from dw.dim_t_user_zip t1
         left join (select *
                    from ods.tbl_users
                    where
                    where dd='2022-10-18'
) t2 on t1.user_id = t2.id;

-- 拉链表有一个弊端 就是每天任务需要正常执行，否则可能会丢失某一天快照
