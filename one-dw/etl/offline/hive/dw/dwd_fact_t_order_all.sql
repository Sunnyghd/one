-- 全量分区存储订单表
insert
overwrite table dw.dwd_fact_t_order partition(dd)
select *
     , current_date as dd
from ods.tbl_orders;