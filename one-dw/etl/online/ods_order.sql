create table t_order_bin
(
    id          int primary key,
    user_sn     string,
    user_phone  string,
    order_sn    string,
    goods_sn    string,
    goods_num   bigint,
    order_price decimal(16, 4),
    create_time timestamp,
    update_time timestamp
) WITH (
      'connector' = 'mysql-cdc',
      'hostname' = 'localhost',
      'port' = '3306',
      'username' = 'root',
      'password' = '123456asd',
      'database-name' = 'biz_order',
      'table-name' = 't_order'
      );


create table ods_order
(
    id          int primary key,
    user_sn     string,
    user_phone  string,
    order_sn    string,
    goods_sn    string,
    goods_num   bigint,
    order_price decimal(16, 4),
    create_time string,
    update_time string,
    ds          string
) PARTITIONED BY (ds)
WITH (
'connector' = 'hudi',
'path' = 'hdfs://one:9000/user/datalake/dw.db/ods_order',
'table.type' = 'MERGE_ON_READ',
'hive_sync.enable'='true',
'hive_sync.table'='ods_order',
'hive_sync.db'='dw',
'hive_sync.mode' = 'hms',
'hive_sync.metastore.uris' = 'thrift://one:9083',
'compaction.async.enabled' = 'true',
'compaction.delta_commits' = '1' -- 为了能在hive快速看到，实际可根据业务吞吐配置一个较大的值
);

insert into ods_order
select id,
       user_sn,
       user_phone,
       order_sn,
       goods_sn,
       goods_num,
       order_price,
       cast(create_time as string)            as create_time,
       cast(update_time as string)            as update_time,
       DATE_FORMAT(create_time, 'yyyy-MM-dd') as ds
from t_order_bin;
