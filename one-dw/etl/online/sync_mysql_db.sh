bin/flink run \
    -c org.apache.paimon.flink.action.FlinkActions \
    lib/paimon-flink-1.16-0.5-SNAPSHOT.jar \
    mysql-sync-database \
    --warehouse hdfs:///user/paimon/warehouse \
    --database datalake \
    --table-prefix ods_ \
    --mysql-conf hostname=one \
    --mysql-conf username=root \
    --mysql-conf password=123456asd \
    --mysql-conf database-name=biz_order \
    --catalog-conf metastore=hive \
    --catalog-conf uri=thrift://one:9083 \
    --table-conf bucket=4 \
    --table-conf changelog-producer=input \
    --table-conf sink.parallelism=4