create table biz_order.t_order
(
    id          int auto_increment comment '自增主键'
        primary key,
    user_sn     varchar(200) null comment '用户编号',
    user_phone  varchar(20) null comment '用户手机号',
    order_sn    varchar(200) null comment '订单编号',
    goods_sn    varchar(200) null comment '商品编号',
    goods_num   bigint null comment '购买数量',
    order_price decimal(16, 4) null comment '订单价格',
    create_time timestamp default CURRENT_TIMESTAMP null,
    update_time timestamp default CURRENT_TIMESTAMP null
) comment '订单表';

create table biz_order.t_goods
(
    id          int auto_increment comment '自增主键'
        primary key,
    goods_sn    varchar(200) null comment '商品编号',
    goods_name  varchar(400) null comment '商品名称',
    orgin_price decimal(16, 4) null comment '商品单价',
    sale_price  decimal(16, 4) null comment '商品售卖价',
    create_time timestamp default CURRENT_TIMESTAMP null,
    update_time timestamp default CURRENT_TIMESTAMP null
) comment '商品表';

