CREATE TABLE IF NOT EXISTS ads_user_sum_d
(
    `user_sn` VARCHAR(200) COMMENT "用户编号",
    `dt` DATE NOT NULL COMMENT "数据灌入日期",
    `order_cnt` BIGINT SUM DEFAULT "0" COMMENT "用户总下单量"
    )
    AGGREGATE KEY(`user_sn`, `dt`)
    DISTRIBUTED BY HASH(`user_sn`) BUCKETS 1
    PROPERTIES (
    "replication_num" = "1"
               );