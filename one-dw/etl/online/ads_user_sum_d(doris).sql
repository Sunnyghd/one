-- 统计每个用户下单量，购买某商品数量 等指标
CREATE CATALOG  myhive WITH (
    'type' = 'paimon',
    'metastore' = 'hive',
    'uri' = 'thrift://one:9083',
    'warehouse' = 'hdfs:///user/paimon/warehouse'
);


--sink
SET
'execution.checkpointing.interval' = '10s';
CREATE TABLE ads_user_sum_d
(
    user_sn   STRING,
    dt        DATE,
    order_cnt BIGINT
)
    WITH (
        'connector' = 'doris',
        'fenodes' = 'one:8050',
        'table.identifier' = 'demo.ads_user_sum_d',
        'username' = 'root',
        'password' = '123456asd',
        'sink.properties.format' = 'json',
        'sink.properties.read_json_by_line' = 'true',
        'sink.enable-delete' = 'true',
        'sink.label-prefix' = 'doris_label'
        );


insert into ads_user_sum_d
select user_sn     as user_sn,
       to_date(from_unixtime(UNIX_TIMESTAMP(cast(create_time as string),'yyyy-MM-dd'),'yyyy-MM-dd')) as dt,
       count(1)    as order_cnt
from myhive.datalake.ods_t_order t1
group by user_sn,to_date(from_unixtime(UNIX_TIMESTAMP(cast(create_time as string),'yyyy-MM-dd'),'yyyy-MM-dd'));

