create table t_goods
(
    id          int
        primary key,
    goods_sn    string,
    goods_name  string,
    orgin_price decimal(16, 4),
    sale_price  decimal(16, 4),
    create_time timestamp,
    update_time timestamp
) WITH (
      'connector' = 'mysql-cdc',
      'hostname' = 'localhost',
      'port' = '3306',
      'username' = 'root',
      'password' = '123456asd',
      'database-name' = 'biz_order',
      'table-name' = 't_goods'
      );


create table ods_goods
(
    id          int
        primary key,
    goods_sn    string,
    goods_name  string,
    orgin_price decimal(16, 4),
    sale_price  decimal(16, 4),
    create_time string,
    update_time string
)
    WITH (
        'connector' = 'hudi',
        'path' = 'hdfs://one:9000/user/datalake/dw.db/ods_goods',
        'table.type' = 'MERGE_ON_READ',
        'hive_sync.enable' = 'true',
        'hive_sync.table' = 'ods_goods',
        'hive_sync.db' = 'dw',
        'hive_sync.mode' = 'hms',
        'hive_sync.metastore.uris' = 'thrift://one:9083',
        'compaction.async.enabled' = 'true',
        'compaction.delta_commits' = '1' -- 为了能在hive快速看到，实际可根据业务吞吐配置一个较大的值
        );

insert into ods_goods
select id,
       goods_sn,
       goods_name,
       orgin_price,
       sale_price,
       cast(create_time as string) as create_time,
       cast(update_time as string) as update_time
from t_goods;
